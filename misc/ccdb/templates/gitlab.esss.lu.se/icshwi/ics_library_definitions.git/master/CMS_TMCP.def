###################################### ICS HWI ###############################################
#############################  ICS Instrument Library     ####################################
##  CCDB device types: ICS_TMCP                                                             ##  
##                                                                                          ##  
##                            TMCP - Target Cryo Plant Interface                            ##
##                                                                                          ##  
##                                                                                          ##
############################         Version: 2.0             ################################
# Author:  Attila Horvath
# Date:    19-04-2023
# Version: v2.0
# Changes: 
# 1. Updated based on LINDE meetings/Implementation   
############################           Version: 1.1           ################################
# Author:  Attila Horvath
# Date:    08-08-2022
# Version: v1.1
# Changes: 
# 1. Archived PVs updated   
############################           Version: 1.0           ################################

# Author:	Miklos Boros 
# Date:		25-03-2022
# Version:  v1.0
##############################################################################################



############################
#  STATUS BLOCK
############################ 
define_status_block()

# TMCP -> CMS Status and Measurements

add_digital("IN_TC31610_Remote",  	ARCHIVE=True,                        PV_DESC="Controller in Remote")
add_digital("IN_TC31610_Local",  	ARCHIVE=True,                        PV_DESC="Controller in Local")

add_digital("IN_Comp1_RUNNING",  	ARCHIVE=True,                        PV_DESC="TMCP_TO_CMS Compressor1 Running")
add_digital("IN_Comp2_RUNNING",  	ARCHIVE=True,                        PV_DESC="TMCP_TO_CMS Compressor2 Running")
add_digital("IN_T31130_RUNNING",   	ARCHIVE=True,                        PV_DESC="TMCP_TO_CMS Turbine T-31130 Running")
add_digital("IN_T31230_RUNNING",   	ARCHIVE=True,                        PV_DESC="TMCP_TO_CMS Turbine T-31230 Running")
add_digital("IN_T31330_RUNNING",   	ARCHIVE=True,                        PV_DESC="TMCP_TO_CMS Turbine T-31330 Running")
add_digital("IN_CD-WU_RDY",  		ARCHIVE=True,                        PV_DESC="TMCP_TO_CMS TMCP ready for CD-WU")
add_digital("IN_Neut_Heat_RDY",  	ARCHIVE=True,                        PV_DESC="TMCP_TO_CMS TMCP ready for neut heating")
add_digital("IN_Hydrogen_Circ_RDY", ARCHIVE=True,                        PV_DESC="TMCP_TO_CMS TMCP ready for hydrogen circ")

add_digital("IN_TMCP_JSB_Mode",  	ARCHIVE=True,                        PV_DESC="TMCP in JSB cold mode")
add_digital("IN_T31130_Sta_Perm",	ARCHIVE=True,                        PV_DESC="T-31130 OK to Start")
add_digital("IN_T31130_Stop_Perm",	ARCHIVE=True,                        PV_DESC="T-31130 OK to Stop")
add_digital("IN_T31230_Sta_Perm",	ARCHIVE=True,                        PV_DESC="T-31230 OK to Start")
add_digital("IN_T31230_Stop_Perm",	ARCHIVE=True,                        PV_DESC="T-31230 OK to Stop")
add_digital("IN_T31330_Sta_Perm",	ARCHIVE=True,                        PV_DESC="T-31330 OK to Start")
add_digital("IN_T31330_Stop_Perm",	ARCHIVE=True,                        PV_DESC="T-31330 OK to Stop")
add_digital("IN_PV31108_Opened",  	ARCHIVE=True,                        PV_DESC="Valve Opened")
add_digital("IN_PV31108_Closed",  	ARCHIVE=True,                        PV_DESC="Valve Closed")
add_digital("IN_PV31208_Opened",  	ARCHIVE=True,                        PV_DESC="Valve Opened")
add_digital("IN_PV31208_Closed",  	ARCHIVE=True,                        PV_DESC="Valve Closed")
add_digital("IN_PV31308_Opened",  	ARCHIVE=True,                        PV_DESC="Valve Opened")
add_digital("IN_PV31308_Closed",  	ARCHIVE=True,                        PV_DESC="Valve Closed")

add_digital("IN_TC51200_Remote",  	ARCHIVE=True,                        PV_DESC="Controller in Remote")
add_digital("IN_TC51200_Local",  	ARCHIVE=True,                        PV_DESC="Controller in Local")

add_digital("IN_Comp1_Trip",  		ARCHIVE=True,                        PV_DESC="Compressor Tripped")
add_digital("IN_Comp2_Trip",  		ARCHIVE=True,                        PV_DESC="Compressor Tripped")

add_digital("IN_T31130_Trip",  		ARCHIVE=True,                        PV_DESC="Turbine Tripped")
add_digital("IN_T31230_Trip",  		ARCHIVE=True,                        PV_DESC="Turbine Tripped")
add_digital("IN_T31330_Trip",  		ARCHIVE=True,                        PV_DESC="Turbine Tripped")

add_digital("IN_Coldbox_Trip",  	ARCHIVE=True,                        PV_DESC="ColdBox Tripped")
add_digital("IN_JSB_Trip",  		ARCHIVE=True,                        PV_DESC="JSB Tripped")

add_digital("IN_PRatioCont_Remote", ARCHIVE=True,                        PV_DESC="Controller in Remote")
add_digital("IN_PRatioCont_Local",  ARCHIVE=True,                        PV_DESC="Controller in Local")

add_digital("IN_PC21700_Remote",  	ARCHIVE=True,                        PV_DESC="Controller in Remote")
add_digital("IN_PC21700_Local",  	ARCHIVE=True,                        PV_DESC="Controller in Local")

add_digital("IN_CV31700_Remote",  	ARCHIVE=True,                        PV_DESC="Controller in Remote")
add_digital("IN_CV31700_Local",  	ARCHIVE=True,                        PV_DESC="Controller in Local")

add_analog("IN_TI31610","REAL",    	ARCHIVE=True,                        PV_DESC="TI-31610",                     PV_EGU="K")
add_analog("IN_TI31750","REAL",    	ARCHIVE=True,                        PV_DESC="TI-31750",                     PV_EGU="K")
add_analog("IN_TI31600","REAL",    	ARCHIVE=True,                        PV_DESC="TI-31600",                     PV_EGU="K")
add_analog("IN_TI51300","REAL",    	ARCHIVE=True,                        PV_DESC="TI-51300",                     PV_EGU="K")
add_analog("IN_FI31100","REAL",    	ARCHIVE=True,                        PV_DESC="FI-31100",                     PV_EGU="g/s")
add_analog("IN_FI31620","REAL",    	ARCHIVE=True,                        PV_DESC="FI-31620",                     PV_EGU="g/s")
add_analog("IN_PI51100","REAL",    	ARCHIVE=True,                        PV_DESC="PI-51100",                     PV_EGU="bara")
add_analog("IN_UY51860","REAL",    	ARCHIVE=True,                        PV_DESC="UY-51860",                     PV_EGU="%")
add_analog("IN_UY51880","REAL",    	ARCHIVE=True,                        PV_DESC="UY-51880",                     PV_EGU="%")
add_analog("IN_PDI51880","REAL",   	ARCHIVE=True,                        PV_DESC="PDI-51880",                    PV_EGU="bara")
add_analog("IN_TI512001","REAL",   	ARCHIVE=True,                        PV_DESC="TI-512001",                    PV_EGU="K")
add_analog("IN_TI512002","REAL",   	ARCHIVE=True,                        PV_DESC="TI-512002",                    PV_EGU="K")
add_analog("IN_PDI51850","REAL",   	ARCHIVE=True,                        PV_DESC="PDI-51850",                    PV_EGU="mbar")
add_analog("IN_PI59100","REAL",    	ARCHIVE=True,                        PV_DESC="PI-59100",                     PV_EGU="mbar")
add_analog("IN_UY51850","REAL",    	ARCHIVE=True,                        PV_DESC="UY-51850",                     PV_EGU="%")
add_analog("IN_UY51870","REAL",    	ARCHIVE=True,                        PV_DESC="UY-51870",                     PV_EGU="%")
add_analog("IN_PI512001","REAL",   	ARCHIVE=True,                        PV_DESC="PI-512001",                    PV_EGU="bara")
add_analog("IN_TI51400","REAL",    	ARCHIVE=True,                        PV_DESC="TI-51400",                     PV_EGU="K")
add_analog("IN_TI33500","REAL",    	ARCHIVE=True,                        PV_DESC="TI-33500",                     PV_EGU="K")
add_analog("IN_PI23400","REAL",    	ARCHIVE=True,                        PV_DESC="PI-23400",                     PV_EGU="bara")
add_analog("IN_PI21700","REAL",    	ARCHIVE=True,                        PV_DESC="PI-21700",                     PV_EGU="bara")
add_analog("IN_PC23400_SP","REAL", 	ARCHIVE=True,                        PV_DESC="PC-23400 Comp Ration SP")
add_analog("IN_UY31840","REAL",    	ARCHIVE=True,                        PV_DESC="UY-31840",                     PV_EGU="%")
add_analog("IN_UY31450","REAL",    	ARCHIVE=True,                        PV_DESC="UY-31450",                     PV_EGU="%")
add_analog("IN_TI33600","REAL",    	ARCHIVE=True,                        PV_DESC="TI-33600",                     PV_EGU="K")

add_analog("IN_UY31130","REAL",    	ARCHIVE=True,                        PV_DESC="UY-31130",                     PV_EGU="%")
add_analog("IN_UY31230","REAL",    	ARCHIVE=True,                        PV_DESC="UY-31230",                     PV_EGU="%")
add_analog("IN_UY31330","REAL",    	ARCHIVE=True,                        PV_DESC="UY-31330",                     PV_EGU="%")

add_analog("IN_TC31750_SP","REAL", 	ARCHIVE=True,                        PV_DESC="TC-31750 Setpoint",            PV_EGU="K")
add_analog("IN_PI33600","REAL",    	ARCHIVE=True,                        PV_DESC="PI-33600",                     PV_EGU="bara")
add_analog("IN_TI31155","REAL",    	ARCHIVE=True,                        PV_DESC="TI-31155",                     PV_EGU="K")
add_analog("IN_TI31255","REAL",    	ARCHIVE=True,                        PV_DESC="TI-31155",                     PV_EGU="K")
add_analog("IN_TI31355","REAL",    	ARCHIVE=True,                        PV_DESC="TI-31155",                     PV_EGU="K")
add_analog("IN_TI31620","REAL",    	ARCHIVE=True,                        PV_DESC="TI-31620",                     PV_EGU="K")
add_analog("IN_PI31620","REAL",    	ARCHIVE=True,                        PV_DESC="PI-31620",                     PV_EGU="bara")
add_analog("IN_PI21701","REAL",    	ARCHIVE=True,                        PV_DESC="PI-21701",                     PV_EGU="bara")
add_analog("IN_PI21750","REAL",    	ARCHIVE=True,                        PV_DESC="PI-21750",                     PV_EGU="bara")
add_analog("IN_UY31700","REAL",    	ARCHIVE=True,                        PV_DESC="UY-31700",                     PV_EGU="%")

add_analog("IN_PC21700_SP","REAL", 	ARCHIVE=True,                        PV_DESC="PC-21700",                     PV_EGU="bara")
add_analog("IN_TC51200_SP","REAL", 	ARCHIVE=True,                        PV_DESC="PC-21700",                     PV_EGU="K")
add_analog("IN_CV51870_SP","REAL", 	ARCHIVE=True,                        PV_DESC="PC-21700",                     PV_EGU="%")

# CMS -> TMCP Status and Commands

add_digital("Out_Heartbeat",  		ARCHIVE=True,                           PV_DESC="CMS_TO_TMCP Heartbeat")
add_digital("Out_CBX_Vac_Fail",  	ARCHIVE=True,                        	PV_DESC="CMS_TO_TMCP CBX Vacuum Failure")
add_digital("Out_Mod_Vac_Fail",  	ARCHIVE=True,                        	PV_DESC="CMS_TO_TMCP Moderator Vacuum Failure")
add_digital("Out_H2_Leak_Fail",  	ARCHIVE=True,                        	PV_DESC="CMS_TO_TMCP H2 Leak Failure")
add_digital("Out_Beam_Inj_Mode",    ARCHIVE=True,                       	PV_DESC="CMS_TO_TMCP Beam Injection Mode")
add_digital("Out_Steady-Stat_Mode", ARCHIVE=True,                    		PV_DESC="CMS_TO_TMCP Steady-State Mode")
add_digital("Out_Standby_Mode",  	ARCHIVE=True,                        	PV_DESC="CMS_TO_TMCP Standby Mode")
add_digital("Out_Cooldn_PhaseI",  	ARCHIVE=True,                       	PV_DESC="CMS_TO_TMCP Cooldown Mode-PhaseI")
add_digital("Out_Cooldn_PhaseII",   ARCHIVE=True,                      		PV_DESC="CMS_TO_TMCP Cooldown Mode-PhaseII")
add_digital("Out_Cooldn_PhaseIII",  ARCHIVE=True,                     		PV_DESC="CMS_TO_TMCP Cooldown Mode-PhaseIII")
add_digital("Out_Warm-up_Mode",  	ARCHIVE=True,                        	PV_DESC="CMS_TO_TMCP Warm-up Mode")
add_digital("Out_Beam_Injection",   ARCHIVE=True,                      		PV_DESC="CMS_TO_TMCP Beam injection signal")
add_digital("Out_Beam_Trip",  		ARCHIVE=True,                           PV_DESC="CMS_TO_TMCP Beam Trip")
add_digital("Out_Warm_He_Sup_Req",  ARCHIVE=True,                     		PV_DESC="CMS_TO_TMCP Warm He supply request")
add_digital("Out_LongCool_Req",     ARCHIVE=True,                     		PV_DESC="CMS_TO_TMCP LongTerm Cool request")

add_analog("Out_Cooling_Power","REAL",  ARCHIVE=True,                    	PV_DESC="CMS_TO_TMCP Actual CMS cooling power",                    	  PV_EGU="kW")
add_analog("Out_CMS_TMCP_Delay","REAL", ARCHIVE=True,                    	PV_DESC="CMS_TO_TMCP Flow Delay",                  	                  PV_EGU="sec")
add_analog("Out_TC31610_SP","REAL",  	ARCHIVE=True,                       PV_DESC="CMS_TO_TMCP TC-31610 Setpoint",                    		  PV_EGU="K")
add_analog("Out_PC21700_SP","REAL",  	ARCHIVE=True,                       PV_DESC="CMS_TO_TMCP PC-21700 Setpoint",                    		  PV_EGU="bara")
add_analog("Out_PC23400_SP","REAL",  	ARCHIVE=True,                       PV_DESC="CMS_TO_TMCP PC-23400 Comp Ratio SP")
add_analog("Out_TC51200_SP","REAL",  	ARCHIVE=True,                       PV_DESC="CMS_TO_TMCP TC-51200 Setpoint",                    		  PV_EGU="K")
add_analog("Out_CV51870_SP","REAL",  	ARCHIVE=True,                       PV_DESC="CMS_TO_TMCP CV-51870 Setpoint",                    		  PV_EGU="%")
add_analog("Out_CV31700_SP","REAL",  	ARCHIVE=True,                       PV_DESC="CMS_TO_TMCP CV-31700 Setpoint",                    		  PV_EGU="%")


# TMCP <-> CMS Communication Status
add_digital("GroupAlarm",                            PV_DESC="Group Alarm for OPI")
add_digital("HeartBeatOK",  		ARCHIVE=True,    PV_DESC="Comm Heartbeat OK")
add_digital("TempReached",                           PV_DESC="Final temperature reached")


add_digital("HeartBeat",  		ARCHIVE=True,                        PV_DESC="Comm. HeartBeat")
add_major_alarm("Comm_Error","Communication Error",                  PV_ZNAM="NominalState")
add_major_alarm("CP_Module_Error","HW CP Module Error",              PV_ZNAM="NominalState")


# CMS ->TMCP Cooldown Controller
add_digital("OpMode_Auto",  	ARCHIVE=True,        PV_DESC="PLC logic operation",        PV_ONAM="True",             PV_ZNAM="False")      
add_digital("OpMode_Manual",  	ARCHIVE=True,        PV_DESC="OPI opreration",             PV_ONAM="True",             PV_ZNAM="False")
add_digital("Enable_Auto",                           PV_DESC="Enable Auto Button",         PV_ONAM="True",             PV_ZNAM="False")
add_digital("Enable_Manual",                         PV_DESC="Enable Manual Button",       PV_ONAM="True",             PV_ZNAM="False")

add_analog("MaxHoldingTemp","REAL",                  PV_DESC="Maximum Holding Temperaure ",              PV_EGU="K")
add_analog("TMCPHX610600","REAL",  	ARCHIVE=True,    PV_DESC="TMCP Temp Difference TI-31610 TI-33600",   PV_EGU="K")
add_analog("TMCPHX750500","REAL",  	ARCHIVE=True,    PV_DESC="TMCP Temp Difference TI-31750 TI-33500",   PV_EGU="K")
add_analog("TMCPHX750600","REAL",  	ARCHIVE=True,    PV_DESC="TMCP Temp Difference TI-31750 TI-31600",   PV_EGU="K")
add_analog("TMCPHX600500","REAL",  	ARCHIVE=True,    PV_DESC="TMCP Temp Difference TI-31600 TI-33500",   PV_EGU="K")

#TC-31610
add_analog("FB_TC31610_RampTime","INT",  		ARCHIVE=True,       PV_DESC="Ramping Cooling Time",                     PV_EGU="min")
add_analog("FB_TC31610_RampTemp","REAL",  		ARCHIVE=True,       PV_DESC="Ramping Cooling Temp.",                    PV_EGU="K")
add_analog("FB_TC31610_HX_HoTemp","REAL" ,  	ARCHIVE=True,       PV_DESC="Holding Temp for HX",                      PV_EGU="K")
add_analog("FB_TC31610_HX_HoTime","INT" ,  		ARCHIVE=True,       PV_DESC="Holding Duration for HX",                  PV_EGU="min")
add_analog("FB_TC31610_SP_HoTemp","REAL" ,  	ARCHIVE=True,       PV_DESC="Holding Temp for SP",               		PV_EGU="K")
add_analog("FB_TC31610_SP_HoTime","INT" ,  		ARCHIVE=True,       PV_DESC="Holding Duration for SP",                  PV_EGU="min")
add_analog("FB_TC31610_CV_HoPos","REAL" ,  		ARCHIVE=True,       PV_DESC="Holding Perc of CV-31450",               	PV_EGU="%")
add_analog("FB_TC31610_CV_HoOff","REAL" ,  		ARCHIVE=True,       PV_DESC="Holding Release Position",                 PV_EGU="%")
add_analog("FB_TC31610_TgtTemp","REAL" ,  		ARCHIVE=True,       PV_DESC="Final Temperature to reach.",              PV_EGU="K")
add_analog("FB_TC31610_MaxSpd","REAL",                      		PV_DESC="Maximum Cooing Speed",                     PV_EGU="K/min")
add_analog("FB_TC31610_ActSpeed","REAL",  		ARCHIVE=True,    	PV_DESC="Actual Cooling Speed",                     PV_EGU="K/min")
add_digital("FB_TC31610_Ramp_NOK",  			ARCHIVE=True,       PV_DESC="Ramp Parameters NOK",      	PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_TC31610_Ramping",  				ARCHIVE=True,       PV_DESC="Ramping Active",             	PV_ONAM="True",				PV_ZNAM="False")
add_digital("En_TC31610_RampON",                         			PV_DESC="Enable RampON Button",       	PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_TC31610_RampOFF",                        			PV_DESC="Enable RampOFF Button",      	PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_TC31610_Hold",                         				PV_DESC="Enable Holding Button",		PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_TC31610_Cancel",                         			PV_DESC="Enable CancelHolding Button",	PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_TC31610_SPReached",				ARCHIVE=True,       PV_DESC="SP Reached",					PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_TC31610_Holding",  				ARCHIVE=True,       PV_DESC="Holding Active",				PV_ONAM="True",             PV_ZNAM="False")
add_string("FB_TC31610_HoRemain", 39,                PV_NAME="FB_TC31610_HoRemain",                 PV_DESC="Holding Time Indication")
add_digital("FB_TC31610_Holding2",  			ARCHIVE=True,       PV_DESC="Holding Active",				PV_ONAM="True",             PV_ZNAM="False")
add_string("FB_TC31610_Ho2Remain", 39,               PV_NAME="FB_TC31610_Ho2Remain",                PV_DESC="Holding Time Indication")
add_analog("FB_TC31610_ActSP","REAL",  			ARCHIVE=True,    	PV_DESC="Actual TMCP Setpoint",                     PV_EGU="K")
add_analog("FB_TC31610_SPDiff","REAL",  		ARCHIVE=True,    	PV_DESC="TMCP SP vs Temp Difference ",     			PV_EGU="K")
add_digital("FB_TC31610_HoldOPI",  				ARCHIVE=True,       PV_DESC="Holding Active",				PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_TC31610_HoldingFR",  			ARCHIVE=True,       PV_DESC="Holding Active",				PV_ONAM="True",             PV_ZNAM="False")
add_analog("FB_TC31610_FR_HoTemp","REAL" ,  	ARCHIVE=True,       PV_DESC="Feed/Return Temp High",                    PV_EGU="K")
add_analog("FB_TC31610_FR_HoTime","INT" ,  		ARCHIVE=True,       PV_DESC="Feed/Return Time High",                  	PV_EGU="min")
add_string("FB_TC31610_HoFRRem", 39,              PV_NAME="FB_TC31610_HoFRRem",                PV_DESC="Holding Time Indication")
add_digital("FB_TC31610_MuteHoFR",  			ARCHIVE=True,       PV_DESC="Holding Muted",				PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_TC31610_Mute",  				ARCHIVE=True,       PV_DESC="Button Enabled",             	PV_ONAM="True",				PV_ZNAM="False")
add_digital("En_TC31610_Unmute",                         			PV_DESC="Button Disabled",       		PV_ONAM="True",             PV_ZNAM="False")
add_analog("FB_TC31610_FR_Diff","REAL" ,  		ARCHIVE=True,       PV_DESC="Feed/Return Temp Diff",                    PV_EGU="K")
add_digital("FB_TC31610_CVHold",  				ARCHIVE=True,       PV_DESC="Holding Active",				PV_ONAM="True",             PV_ZNAM="False")
add_string("FB_TC31610_CVHoRem", 39,                PV_NAME="FB_TC31610_CVHoRem",                 PV_DESC="Holding Pos Indication")


#TC-51200
add_analog("FB_TC51200_RampTime","INT",  		ARCHIVE=True,       PV_DESC="Ramping Cooling Time",                     PV_EGU="min")
add_analog("FB_TC51200_RampTemp","REAL",  		ARCHIVE=True,       PV_DESC="Ramping Cooling Temp.",                    PV_EGU="K")
add_analog("FB_TC51200_CV_HoTemp","REAL" ,  	ARCHIVE=True,       PV_DESC="Holding Temp for CV",                      PV_EGU="%")
add_analog("FB_TC51200_CV_HoTime","INT" ,  		ARCHIVE=True,       PV_DESC="Holding Duration for CV",                  PV_EGU="min")
add_analog("FB_TC51200_SP_HoTemp","REAL" ,  	ARCHIVE=True,       PV_DESC="Holding Temp for SP",               		PV_EGU="K")
add_analog("FB_TC51200_SP_HoTime","INT" ,  		ARCHIVE=True,       PV_DESC="Holding Duration for SP",                  PV_EGU="min")
add_analog("FB_TC51200_TgtTemp","REAL" ,  		ARCHIVE=True,       PV_DESC="Final Temperature to reach.",              PV_EGU="K")
add_analog("FB_TC51200_MaxSpd","REAL",                      		PV_DESC="Maximum Cooing Speed",                     PV_EGU="K/min")
add_analog("FB_TC51200_ActSpeed","REAL",  		ARCHIVE=True,    	PV_DESC="Actual Cooling Speed",                     PV_EGU="K/min")
add_digital("FB_TC51200_Ramp_NOK",  			ARCHIVE=True,       PV_DESC="Ramp Parameters NOK",      	PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_TC51200_Ramping",  				ARCHIVE=True,       PV_DESC="Ramping Active",             	PV_ONAM="True",				PV_ZNAM="False")
add_digital("En_TC51200_RampON",                         			PV_DESC="Enable RampON Button",       	PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_TC51200_RampOFF",                        			PV_DESC="Enable RampOFF Button",      	PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_TC51200_Hold",                         				PV_DESC="Enable Holding Button",		PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_TC51200_Cancel",                         			PV_DESC="Enable CancelHolding Button",	PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_TC51200_SPReached",				ARCHIVE=True,       PV_DESC="SP Reached",					PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_TC51200_Holding",  				ARCHIVE=True,       PV_DESC="Holding Active",				PV_ONAM="True",             PV_ZNAM="False")
add_string("FB_TC51200_HoRemain", 39,                PV_NAME="FB_TC51200_HoRemain",                 PV_DESC="Holding Time Indication")
add_digital("FB_TC51200_Holding2",  			ARCHIVE=True,       PV_DESC="Holding Active",				PV_ONAM="True",             PV_ZNAM="False")
add_string("FB_TC51200_Ho2Remain", 39,               PV_NAME="FB_TC51200_Ho2Remain",                PV_DESC="Holding Time Indication")
add_analog("FB_TC51200_ActSP","REAL",  			ARCHIVE=True,    	PV_DESC="Actual TMCP Setpoint",                     PV_EGU="K")
add_analog("FB_TC51200_SPDiff","REAL",  		ARCHIVE=True,    	PV_DESC="TMCP SP vs Temp Difference ",     			PV_EGU="K")
add_digital("FB_TC51200_HoldOPI",  				ARCHIVE=True,       PV_DESC="Holding Active",				PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_TC51200_RegON",  				ARCHIVE=True,       PV_DESC="Regulation ON",				PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_TC51200_RegOFF",  				ARCHIVE=True,       PV_DESC="Regulation OFF",				PV_ONAM="True",             PV_ZNAM="False")
add_analog("FB_CV51870_SP","REAL" ,  			ARCHIVE=True,       PV_DESC="Vavle Setpoint",              				PV_EGU="%")
add_digital("FB_CV51870_Frozen",                         			PV_DESC="CV Output Frozen",				PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_CV51870_Freeze",                         			PV_DESC="Enable Freeze Button",			PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_CV51870_UnFreeze",                         			PV_DESC="Enable Freeze Button",			PV_ONAM="True",             PV_ZNAM="False")


#CV31700
add_analog("FB_CV31700_SP","REAL" ,  			ARCHIVE=True,       PV_DESC="Vavle Setpoint",              				PV_EGU="%")


#Alarms
add_major_alarm("HX610600_High",					 "TI-31610/33600 Dif High",  		 				PV_ZNAM="NominalState")
add_major_alarm("HX750500_High",					 "TI-31750/33500 Dif High",  						PV_ZNAM="NominalState")
add_major_alarm("HX750600_High",					 "TI-31750/31600 Dif High",  						PV_ZNAM="NominalState")
add_major_alarm("HX600500_High",					 "TI-31600/33500 Dif High",  						PV_ZNAM="NominalState")


#CMS -> TMCP HP/Compression Ratio Control
add_analog("FB_LP_HoldPres","REAL",  			ARCHIVE=True,       PV_DESC="LP Start Pressure",                     	PV_EGU="bara")
add_analog("FB_HP_Final","REAL",  				ARCHIVE=True,       PV_DESC="HP Final Pressure",                     	PV_EGU="bara")
add_analog("FB_HP_RampTime","INT",  			ARCHIVE=True,       PV_DESC="HP Ramp Time",                     		PV_EGU="min")
add_analog("FB_HP_RampPres","REAL",  			ARCHIVE=True,       PV_DESC="HP Ramp Pressure",                     	PV_EGU="bara")
add_analog("FB_CR_Final","REAL",  				ARCHIVE=True,       PV_DESC="Final Compression Ratio")
add_analog("FB_HP_SP_HoPres","REAL" ,  			ARCHIVE=True,       PV_DESC="HP Holding Pressure for SP",               PV_EGU="bara")
add_analog("FB_HP_SP_HoTime","INT" ,  			ARCHIVE=True,       PV_DESC="HP Holding Duration for SP",               PV_EGU="min")
add_analog("FB_CR_SP_HoPres","REAL" ,  			ARCHIVE=True,       PV_DESC="CR Holding Pressure for SP")
add_analog("FB_CR_SP_HoTime","INT" ,  			ARCHIVE=True,       PV_DESC="CR Holding Duration for SP",               PV_EGU="min")
add_analog("FB_CR_Calc","REAL" ,  				ARCHIVE=True,       PV_DESC="Calculated CR")

add_analog("MaxCompRatio","REAL",                    PV_DESC="Maximum Compression Ratio")
add_analog("MinCompRatio","REAL",                    PV_DESC="Minumum Compression Ratio")
add_analog("MaxDischarge","REAL",                    PV_DESC="Maximum HP Pressure",            							PV_EGU="bara")
add_analog("MinDischarge","REAL",                    PV_DESC="Minimum HP Pressure",            							PV_EGU="bara")
add_analog("MinLowPressure","REAL",                  PV_DESC="Minimum LP Pressure",            							PV_EGU="bara")

add_analog("FB_PC-21700_SP","REAL",	ARCHIVE=True,	PV_DESC="Compressor Discharge Pressure",            PV_EGU="bara")
add_analog("FB_PC-23400_SP","REAL", ARCHIVE=True,	PV_DESC="Compressor Compression Ratio")

add_digital("En_HP_CR_Cancel",                      PV_DESC="Enable Start Button",	PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_HP_CR_Start",                       PV_DESC="Enable Start Button",	PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_HP_Start",                       PV_DESC="Enable Start Button",	PV_ONAM="True",             PV_ZNAM="False")
add_digital("En_HP_CR_Stop",                       	PV_DESC="Enable Stop Button",	PV_ONAM="True",             PV_ZNAM="False")

add_digital("FB_HPCR_RampEna",                      PV_DESC="Ramping enabled",		PV_ONAM="True",             PV_ZNAM="False")
add_analog("FB_HP_JumpSP","REAL",   ARCHIVE=True,  	PV_DESC="HP Jump Feedback",                      	PV_EGU="bara")
add_analog("FB_CR_JumpSP","REAL",	ARCHIVE=True,  	PV_DESC="CR Jump Feedback")
add_digital("FB_HP_Final_OK",                       PV_DESC="Parameter OK",			PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_HP_Ramp_OK",                       	PV_DESC="Parameter OK",			PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_LP_Hold_OK",                        PV_DESC="Parameter OK",			PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_CR_Final_OK",                       PV_DESC="Parameter OK",			PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_HP_SP_Hold",                        PV_DESC="HP Setpoint Discepency",			PV_ONAM="True",             PV_ZNAM="False")
add_string("FB_HP_SP_HoldRem", 39,                	PV_NAME="FB_HP_SP_HoldRem",                 PV_DESC="Holding Time Remaining")
add_digital("FB_CR_SP_Hold",                        PV_DESC="HP Setpoint Discepency",			PV_ONAM="True",             PV_ZNAM="False")
add_string("FB_CR_SP_HoldRem", 39,                	PV_NAME="FB_CR_SP_HoldRem",                 PV_DESC="Holding Time Remaining")
add_digital("FB_HP_CR_Para_OK",                     PV_DESC="HP/CR Parameters OK",				PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_HP_CR_Ramping", 	ARCHIVE=True,	PV_DESC="HP/CR Ramping",      				PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_HP_Ramping", 		ARCHIVE=True,	PV_DESC="HP/CR Ramping",      				PV_ONAM="True",             PV_ZNAM="False")
add_analog("FB_HP_ActSpeed","REAL", ARCHIVE=True,  PV_DESC="Actual Ramping Speed",                     PV_EGU="bara/min")
add_analog("FB_HP_MaxSpeed","REAL", ARCHIVE=True,  PV_DESC="Actual Ramping Speed",                     PV_EGU="bara/min")

add_analog("FB_LP_PermPres","REAL",	ARCHIVE=True,  	PV_DESC="Ramping Permissive",                      	PV_EGU="bara")
add_analog("FB_LP_PermDB","REAL",   ARCHIVE=True,  	PV_DESC="Ramping Permissive",                      	PV_EGU="bara")
add_analog("FB_LP_StartPres","REAL",ARCHIVE=True,  	PV_DESC="Start Pressure",                      		PV_EGU="bara")
add_analog("FB_CR_Perm","REAL", 	ARCHIVE=True,	PV_DESC="Comp Ratio Permissive")
add_analog("FB_CR_PermDB","REAL", 	ARCHIVE=True,	PV_DESC="Comp Ratio Permissive")
add_analog("FB_HP_PermDB","REAL",   ARCHIVE=True,  	PV_DESC="HP Permissive",                      		PV_EGU="bara")
add_digital("FB_HP_PermOK",                      	PV_DESC="HP Permissive OK",		PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_LP_PermOK",                      	PV_DESC="LP Permissive OK",		PV_ONAM="True",             PV_ZNAM="False")
add_digital("FB_CR_PermOK",                      	PV_DESC="CR Permissive OK",		PV_ONAM="True",             PV_ZNAM="False")

#added as spare not in use yet
add_analog("FB_LP_Final","REAL",					ARCHIVE=True,  		PV_DESC="LP Final Setpoint",                      	PV_EGU="bara")


#Enable/Disable Buttons
add_digital("ENA_Start_T31130",  	ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_Stop_T31130",  	ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_Start_T31230",  	ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_Stop_T31230",  	ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_Start_T31330",  	ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_Stop_T31330",  	ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_Use_HPBuff",  		ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_Use_LPBuff",  		ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_CV51870_Auto",  	ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_CV51870_Manual",  	ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_CV31700_Auto",  	ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_CV31700_Manual",  	ARCHIVE=True,    PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")


add_digital("ENA_TC31610_JUMP",  					 PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_TC51200_JUMP",  					 PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_HP_JUMP",  		    			 PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_CR_JUMP",  		    			 PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")
add_digital("ENA_Start_HPRamp",  		    		 PV_DESC="Enable Button",        PV_ONAM="True",             PV_ZNAM="False")




############################
#  COMMAND BLOCK
############################ 
define_command_block()

#OPI buttons
add_digital("Cmd_Auto",                           		PV_DESC="CMD: Auto Mode")
add_digital("Cmd_Manual",                         		PV_DESC="CMD: Manual Mode")

add_digital("Cmd_AckAlarm",					           	PV_DESC="CMD: Acknowledge Alarm")

#Controller Commands
add_digital("Cmd_TC31610_RampON",   	                PV_DESC="CMD: Ramping ON")
add_digital("Cmd_TC31610_RampOFF",	                    PV_DESC="CMD: Ramping OFF")
add_digital("Cmd_TC31610_HoldON",   	                PV_DESC="CMD: Holding ON")
add_digital("Cmd_TC31610_HoldOFF",	                    PV_DESC="CMD: Cancel Holding")
add_digital("Cmd_TC31610_MuteFR",   	                PV_DESC="CMD: Mute FR Holding")
add_digital("Cmd_TC31610_UnmuteFR",	                    PV_DESC="CMD: Unmute FR Holding")
add_digital("Cmd_TC51200_RampON",   	                PV_DESC="CMD: Ramping ON")
add_digital("Cmd_TC51200_RampOFF",	                    PV_DESC="CMD: Ramping OFF")
add_digital("Cmd_TC51200_HoldON",   	                PV_DESC="CMD: Holding ON")
add_digital("Cmd_TC51200_HoldOFF",	                    PV_DESC="CMD: Cancel Holding")
add_digital("Cmd_TC31610_Jump",   	                	PV_DESC="CMD: Jump to SP")
add_digital("Cmd_TC51200_Jump",   	                	PV_DESC="CMD: Jump to SP")

#Turbine Commands
add_digital("Cmd_Start_T31130",                         PV_DESC="CMD: Start Turbine")
add_digital("Cmd_Stop_T31130",                         	PV_DESC="CMD: Stop Turbine")
add_digital("Cmd_Start_T31230",                         PV_DESC="CMD: Start Turbine")
add_digital("Cmd_Stop_T31230",                         	PV_DESC="CMD: Stop Turbine")
add_digital("Cmd_Start_T31330",                         PV_DESC="CMD: Start Turbine")
add_digital("Cmd_Stop_T31330",                         	PV_DESC="CMD: Stop Turbine")

#HP/LP Buffer
add_digital("Cmd_Use_HPBuff",                         	PV_DESC="CMD: Use HP Buffer")
add_digital("Cmd_Use_LPBuff",                         	PV_DESC="CMD: Use LP Buffer")

#Valve Manual/Auto Mode
add_digital("Cmd_CV51870_Auto",                         PV_DESC="CMD: Auto Mode")
add_digital("Cmd_CV51870_Manual",                       PV_DESC="CMD: Manual Mode")
add_digital("Cmd_CV31700_Auto",                         PV_DESC="CMD: Auto Mode")
add_digital("Cmd_CV31700_Manual",                       PV_DESC="CMD: Manual Mode")
add_digital("Cmd_CV51870_Freeze",                       PV_DESC="CMD: Freeze Valve")
add_digital("Cmd_CV51870_UnFreeze",                     PV_DESC="CMD: Unfreeze Valve")

#HP/CR Commands
add_digital("Cmd_Start_HPCRRamp",                       PV_DESC="CMD: Start Ramping")
add_digital("Cmd_Start_HPRamp",                         PV_DESC="CMD: Start Ramping")
add_digital("Cmd_Stop_HPCRRamp",                        PV_DESC="CMD: Stop Ramping")
add_digital("Cmd_HP_CR_HoldOFF",	                    PV_DESC="CMD: Cancel Holding")
add_digital("Cmd_HPJumpSP",	                    		PV_DESC="CMD: Jump to setpoint")
add_digital("Cmd_CRJumpSP",	                    		PV_DESC="CMD: Jump to setpoint")


############################
#  PARAMETER BLOCK
############################ 
define_parameter_block()

# CMS ->TMCP Cooldown Controller TC-31610 (Supply temperature control)

add_analog("P_TC31610_RampTime","INT",  		ARCHIVE=True,       PV_DESC="Ramping Cooling Time",                     PV_EGU="min")
add_analog("P_TC31610_RampTemp","REAL",  		ARCHIVE=True,       PV_DESC="Ramping Cooling Temp.",                    PV_EGU="K")
add_analog("P_TC31610_HX_HoTemp","REAL" ,  		ARCHIVE=True,       PV_DESC="Holding Temp for HX",                      PV_EGU="K")
add_analog("P_TC31610_HX_HoTime","INT" ,  		ARCHIVE=True,       PV_DESC="Holding Duration for HX",                  PV_EGU="min")
add_analog("P_TC31610_SP_HoTemp","REAL" ,  		ARCHIVE=True,       PV_DESC="Holding Temp for SP",               		PV_EGU="K")
add_analog("P_TC31610_SP_HoTime","INT" ,  		ARCHIVE=True,       PV_DESC="Holding Duration for SP",                  PV_EGU="min")
add_analog("P_TC31610_TgtTemp","REAL" ,  		ARCHIVE=True,       PV_DESC="Final Temperature to reach.",              PV_EGU="K")
add_analog("P_TC31610_FR_HoTemp","REAL" ,  		ARCHIVE=True,       PV_DESC="Feed/Return Temp High",                    PV_EGU="K")
add_analog("P_TC31610_FR_HoTime","INT" ,  		ARCHIVE=True,       PV_DESC="Feed/Return Time High",                  	PV_EGU="min")
add_analog("P_TC31610_CV_HoPos","REAL" ,  		ARCHIVE=True,       PV_DESC="Holding Pos for CV-31450",                	PV_EGU="%")
add_analog("P_TC31610_CV_HoOff","REAL" ,  		ARCHIVE=True,       PV_DESC="Holding Release Position",                 PV_EGU="%")

# CMS ->TMCP Cooldown Controller TC-51200 (Return temperature control)

add_analog("P_TC51200_RampTime","INT",  		ARCHIVE=True,       PV_DESC="Ramping Cooling Time",                     PV_EGU="min")
add_analog("P_TC51200_RampTemp","REAL",  		ARCHIVE=True,       PV_DESC="Ramping Cooling Temp.",                    PV_EGU="K")
add_analog("P_TC51200_CV_HoPos","REAL" ,  		ARCHIVE=True,       PV_DESC="Holding Pos for CV-31450",                	PV_EGU="%")
add_analog("P_TC51200_CV_HoTime","INT" ,  		ARCHIVE=True,       PV_DESC="Holding Duration",                         PV_EGU="min")
add_analog("P_TC51200_SP_HoTemp","REAL" ,  		ARCHIVE=True,       PV_DESC="Holding Temp for SP",               		PV_EGU="K")
add_analog("P_TC51200_SP_HoTime","INT" ,  		ARCHIVE=True,       PV_DESC="Holding Duration for SP",                  PV_EGU="min")
add_analog("P_TC51200_TgtTemp","REAL" ,  		ARCHIVE=True,       PV_DESC="Final Temperature to reach.",              PV_EGU="K")
add_analog("P_CV51870_SP","REAL" ,  			ARCHIVE=True,       PV_DESC="Vavle Setpoint",              				PV_EGU="%")

#HP/LP Control
add_analog("P_HP_Final","REAL",  				ARCHIVE=True,       PV_DESC="HP Final Pressure",                     	PV_EGU="bara")
add_analog("P_HP_RampTime","INT",  				ARCHIVE=True,       PV_DESC="HP Ramp Time",                     		PV_EGU="min")
add_analog("P_HP_RampPres","REAL",  			ARCHIVE=True,       PV_DESC="HP Ramp Pressure",                     	PV_EGU="bara")
add_analog("P_CR_Final","REAL",  				ARCHIVE=True,       PV_DESC="Final Compression Ratio")
add_analog("P_HP_SP_HoPres","REAL" ,  			ARCHIVE=True,       PV_DESC="HP Holding Pressure for SP",               PV_EGU="bara")
add_analog("P_HP_SP_HoTime","INT" ,  			ARCHIVE=True,       PV_DESC="HP Holding Duration for SP",               PV_EGU="min")
add_analog("P_CR_SP_HoPres","REAL" ,  			ARCHIVE=True,       PV_DESC="CR Holding Pressure for SP")
add_analog("P_CR_SP_HoTime","INT" ,  			ARCHIVE=True,       PV_DESC="CR Holding Duration for SP",               PV_EGU="min")

add_analog("P_LP_PermPres","REAL",				ARCHIVE=True,  		PV_DESC="Ramping Permissive",                      	PV_EGU="bara")
add_analog("P_LP_PermDB","REAL",   				ARCHIVE=True,  		PV_DESC="Ramping Permissive",                      	PV_EGU="bara")
add_analog("P_HP_PermPres","REAL",				ARCHIVE=True,  		PV_DESC="Ramping Permissive",                      	PV_EGU="bara")
add_analog("P_HP_PermDB","REAL",   				ARCHIVE=True,  		PV_DESC="Ramping Permissive",                      	PV_EGU="bara")
add_analog("P_CR_Perm","REAL", 					ARCHIVE=True,		PV_DESC="Comp Ratio Permissive")
add_analog("P_CR_PermDB","REAL", 				ARCHIVE=True,		PV_DESC="Comp Ratio Permissive")

add_analog("P_HP_JumpSP","REAL",				ARCHIVE=True,  		PV_DESC="HP Jump Setpoint",                      	PV_EGU="bara")
add_analog("P_CR_JumpSP","REAL",				ARCHIVE=True,  		PV_DESC="CR Jump Setpoint")

#added as spare not in use yet
add_analog("P_LP_Final","REAL",					ARCHIVE=True,  		PV_DESC="LP Final Setpoint",                      	PV_EGU="bara")

#CV31700
add_analog("P_CV31700_SP","REAL" ,  			ARCHIVE=True,       PV_DESC="Vavle Setpoint",              				PV_EGU="%")