# Startup for CrS-CMS:SC-IOC-001

# Load required modules
require essioc
require s7plc
require modbus
require calc

# Load standard IOC startup scripts
iocshLoad("$(essioc_DIR)/common_config.iocsh")

# Load PLC specific startup script
iocshLoad("$(E3_CMD_TOP)/iocsh/crs_cms_ctrl_plc_001.iocsh", "DBDIR=$(E3_CMD_TOP)/db/, MODVERSION=$(IOCVERSION=)")

